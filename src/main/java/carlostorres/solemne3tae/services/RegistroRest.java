/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package carlostorres.solemne3tae.services;

import carlostorres.solemne3tae.dao.RegistroJpaController;
import carlostorres.solemne3tae.dao.exceptions.NonexistentEntityException;
import carlostorres.solemne3tae.entity.Registro;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 *
 * @author Carlos
 */
@Path("registro")
public class RegistroRest {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarPeresona() {
        RegistroJpaController daoRegistro = new RegistroJpaController();
        List<Registro> listaPersona = daoRegistro.findRegistroEntities();
        return Response.ok(200).entity(listaPersona).build();
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response crearPersona(Registro datosRegistro) {
        RegistroJpaController daoRegistro = new RegistroJpaController();
        try {
            daoRegistro.create(datosRegistro);
        } catch (Exception ex) {
            Logger.getLogger(RegistroRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(datosRegistro).build();
    }

    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response editarPersona(Registro datosRegistro) {
        RegistroJpaController daoRegistro = new RegistroJpaController();
        try {
            daoRegistro.edit(datosRegistro);
        } catch (Exception ex) {
            Logger.getLogger(RegistroRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(datosRegistro).build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")//recibe rut
    public Response buscarPersona(@PathParam("rut") String rut)
    {
        RegistroJpaController daoRegistro = new RegistroJpaController();
        Registro datosRegistro = daoRegistro.findRegistro(rut);
        return Response.ok(200).entity(datosRegistro).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{rut}")
    public Response eliminarPersona(@PathParam("rut") String rut)  
    {
        RegistroJpaController daoRegistro = new RegistroJpaController();
        try {
            daoRegistro.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(RegistroRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity("{\"Atencion \":\"registro eliminado \"}").build();
    }

}
