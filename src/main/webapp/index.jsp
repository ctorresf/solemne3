<%-- 
    Document   : index
    Created on : 01-07-2021, 20:28:19
    Author     : Carlos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Registro</title>
        <script type="text/javascript" src="js/validaciones.js"></script>
        <link type="text/css" rel="stylesheet" href="css/estilos.css" media="screen" /></link>
    </head>
    <body>
        <form name="frmRegistro" id="frmRegistro" action="RegistroController" method="POST">
            <table border="0" cellspacing="0" cellpadding="5" align="center" width="700" height="300">
                <tr>
                    <td align="center">
                        <fieldset>
                            <legend>Solemne 3</legend>
                            <table class="table extBorder" align="center" cellpadding="2" cellspacing="1" border="0" width="800">
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Listar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-tae.herokuapp.com/api/registro" target="_blank">https://solemne3-tae.herokuapp.com/api/registro (ejemplo)</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Crear:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-tae.herokuapp.com/api/registro" target="_blank">https://solemne3-tae.herokuapp.com/api/registro (ejemplo)</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Editar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-tae.herokuapp.com/api/registro" target="_blank">https://solemne3-tae.herokuapp.com/api/registro (ejemplo)</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Buscar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-tae.herokuapp.com/api/registro/25705698-8" target="_blank">https://solemne3-tae.herokuapp.com/api/registro/25705698-8 (ejemplo)</a>
                                </tr>
                                <tr>
                                    <td align="right" class="tituloItem">
                                        Eliminar:
                                    </td>
                                    <td>
                                        <a href="https://solemne3-tae.herokuapp.com/api/registro/12345678-9" target="_blank">https://solemne3-tae.herokuapp.com/api/registro/12345678-9 (ejemplo)</a>
                                    </td>
                                </tr>
                            </table>
                            <br>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
