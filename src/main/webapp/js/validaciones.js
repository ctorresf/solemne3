/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function fnEditar(rut) {
    window.location.href = 'EditarController?rut=' + rut;
}
function fnVolverListado() {
    window.location.href = 'ListarController';
}
function fnHome() {
    window.location.href = 'index.jsp';
}

function fnLimpiarRegistro() {
    document.getElementById('txtRut').value = '';
    document.getElementById('txtUsuario').value = '';
    document.getElementById('txtContrasena').value = '';
    document.getElementById('txtEdad').value = '';
    document.getElementById('txtCorreo').value = '';
}
